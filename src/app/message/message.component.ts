import { Component, OnInit } from '@angular/core';
import {Message} from '../message';

@Component({
  selector: 'app-message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.css']
})
export class MessageComponent implements OnInit {

  constructor() {
    this.author = "";
    this.text = "";
  }
  author: string;
  text: string;

  static parse(message: Message): MessageComponent {
    const msgcom = new MessageComponent();
    msgcom.author = message.author;
    msgcom.text = message.text;
    return msgcom;
  }

  ngOnInit(): void {
  }

}

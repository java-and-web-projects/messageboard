import { Component, OnInit } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {ConversationsComponent} from '../conversations/conversations.component';
import {Message} from '../message';

@Component({
  selector: 'app-text-box',
  templateUrl: './text-box.component.html',
  styleUrls: ['./text-box.component.css']
})
export class TextBoxComponent implements OnInit {
  model: Message = new Message();

  constructor(private htttp: HttpClient) { }

  ngOnInit(): void{
  }

  displayMessage(): void {
    let httpHeaders: HttpHeaders = new HttpHeaders({ 'Content-Type': 'application/json' });
    let options = {
      headers: httpHeaders
    };
    let url = 'http://localhost:8080/';

    let message = new Message();
    message.author = this.model.author;
    message.text = this.model.text;
    ConversationsComponent.messages.push(message);
    alert(message.author);
     this.htttp.post(url, this.model, options).subscribe(
       res => {alert('okk');},
       err => { alert('on post error occurred'); }
    );
  }


}

import { Component, OnInit } from '@angular/core';
import {Message} from '../message';
import {HttpClient} from '@angular/common/http';

@Component({
  selector: 'app-conversations',
  templateUrl: './conversations.component.html',
  styleUrls: ['./conversations.component.css']
})
export class ConversationsComponent implements OnInit {
  static messages: Message[] = [new Message()];
  constructor(private http: HttpClient) {
    let url = 'http://localhost:8080/';
    http.get<Message[]>(url).subscribe(
      res => ConversationsComponent.messages = res,
      error => (ConversationsComponent.messages.push(new Message()))
    );
  }

  ngOnInit(): void {
  }

  getMessages(): Message[] {
    return ConversationsComponent.messages;
  }
}

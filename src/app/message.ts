import {MessageComponent} from './message/message.component';

export class Message {
  author: string;
  text: string;

  constructor() {

  }

  getComponenet(): MessageComponent{
    return MessageComponent.parse(this);
  }

}

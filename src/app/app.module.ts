import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { TextBoxComponent } from './text-box/text-box.component';
import { ConversationsComponent } from './conversations/conversations.component';
import { LoginFormComponent } from './login-form/login-form.component';
import {Router, RouterModule, Routes} from '@angular/router';
import {FormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import { MessageComponent } from './message/message.component';

const boxRoute: Routes = [
  {
    path: 'message',
    component: LoginFormComponent
  },
  {
    path: '',
    component: TextBoxComponent
  }
];
const conversationRoute: Routes = [
  {
    path: 'messages',
    component: LoginFormComponent
  },
  {
    path: 'messages',
    component: ConversationsComponent
  }
];


@NgModule({
  declarations: [
    AppComponent,
    TextBoxComponent,
    ConversationsComponent,
    LoginFormComponent,
    MessageComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    RouterModule.forRoot(boxRoute)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
